local _G = _G
local AttributeStoneExchangeCache = {}
_G.AttributeStoneExchangeCache = AttributeStoneExchangeCache

AttributeStoneExchangeCache.current_money = nil

local index_cache = {}
AttributeStoneExchangeCache.index_cache = index_cache

local id_cache = {}
AttributeStoneExchangeCache.id_cache = id_cache

local original_FusionStoneTradeFrame_OnShow = _G.FusionStoneTradeFrame_OnShow
local original_FusionStoneAttr_SetSelection = _G.FusionStoneAttr_SetSelection
local original_StoreMoneyFrame_SetMoney = _G.StoreMoneyFrame_SetMoney

local function decorated_StoreMoneyFrame_SetMoney(frameName, moneyType, money)
    if moneyType == 0 then
        AttributeStoneExchangeCache.current_money = money
    end
    original_StoreMoneyFrame_SetMoney(frameName, moneyType, money)
end

local function decorated_FusionStoneTradeFrame_OnShow()
    original_FusionStoneTradeFrame_OnShow(nil)

    local current_money = AttributeStoneExchangeCache.current_money
    if current_money ~= nil and index_cache[current_money] ~= nil then
        for i = 1, 3 do
            local index = index_cache[current_money][i]
            if index ~= nil then
                local id = id_cache[current_money][i]
                _G.g_FST_AttrItemSelected_Indices[i] = index;
                _G.g_FST_AttrItemChecked_Index = index
                _G.FusionStoneAttr_SetSelection( id, i );
            end
        end
    end
end

local function decorated_FusionStoneAttr_SetSelection(attrId, attrFrameId)
    if attrId == 0 then
        return
    end
    local current_money = AttributeStoneExchangeCache.current_money
    if current_money ~= nil then
        if index_cache[current_money] == nil then
            index_cache[current_money] = {}
            id_cache[current_money] = {}
        end

        index_cache[current_money][attrFrameId] = _G.g_FST_AttrItemChecked_Index
        id_cache[current_money][attrFrameId] = attrId
    end

    original_FusionStoneAttr_SetSelection(attrId, attrFrameId)
end

_G.FusionStoneTradeFrame_OnShow = decorated_FusionStoneTradeFrame_OnShow
_G.FusionStoneAttr_SetSelection = decorated_FusionStoneAttr_SetSelection
_G.StoreMoneyFrame_SetMoney = decorated_StoreMoneyFrame_SetMoney

local FusionStoneTradeFrameOkButton_OnClick = FusionStoneTradeFrameOkButton_OnClick
local UseSkill = UseSkill
local ChoiceOption = ChoiceOption
function FST_Buy()
    FusionStoneTradeFrameOkButton_OnClick()
    UseSkill(1,1)
    ChoiceOption(1)
end